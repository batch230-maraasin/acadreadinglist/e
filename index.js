console.log("The sum of numA and numB should be 5");
let numA = 2;
let numB = 3;
let sum = numA + numB;
console.log("Code Output: " + sum);
console.log("Sum of numA and numB:" + sum);

console.log("The difference of numA minus numB should be 1");
numA = 6;
numB = 5;
let difference = numA - numB;
console.log("Code Output: " + difference);
console.log("Difference of numA and numB:" + difference);

console.log("The product of numA times numB should be 12");
numA = 4;
numB = 3;
let product = numA * numB;
console.log("Code Output: " + product);
console.log("Product of numA and numB:" + product);

console.log("The quotient of numA divided by numB should be 2");
numA = 6;
numB = 3;
let quotient = numA / numB;
console.log("Code Output: " + quotient);
console.log("Quotient of numA and numB:" + quotient);